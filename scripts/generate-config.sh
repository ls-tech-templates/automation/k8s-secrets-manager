#!/bin/bash
#
# This script generate Kubernetes Deployment config
# based on GitLAB variables or default predefined values
CONFIG=${1:-config.json}
TEMPLATE_DIR=templates

# Default values if not exist from GitLab Environment
APPNAME=${APPNAME:-secret-manager}
NAMESPACENAME=${KUBE_NAMESPACE:-default}
VAULT_ADDRESS=${VAULT_ADDRESS:-https://vault:8200}
CI_PIPELINE_ID=${CI_PIPELINE_ID:-undefined}
CI_ENVIRONMENT_SLUG=${CI_ENVIRONMENT_SLUG:-undefined}
CLUSTERWIDE=${CLUSTERWIDE:-yes}

# Check required Variables
if [[ "$APPROLE_ID" == "" ]] ; then
    echo "You need to set APPROLE_ID variable"
    exit 1
else
    APPROLE_ID_HASH=$(echo -n $APPROLE_ID | base64)
fi

if [[ "$APPROLE_SECRET_ID" == "" ]] ; then 
    echo "You need to set APPROLE_SECRET_ID variable"
    exit 1
else
    APPROLE_SECRET_ID_HASH=$(echo -n $APPROLE_SECRET_ID | base64)
fi


# Exit if any error
set -e

# Generate ConfigMap Data
#
# Output should be like this
#
#    - name: cert-domain.tld
#      type: kubernetes.io/tls
#      namespaces: ["default"]
#      data:
#        tls.crt:
#          path: certs/domain.tld
#          key: crt
#        tls.key:
#          path: certs/domain.tld
#          key: key
CONFIG_MAP_DATA=""
echo "ConfigMap Data generate from $CONFIG"
for secret in $(jq -r .vault_secrets[] $CONFIG) ; do

    secret_name=$(basename $secret | sed 's/[_:,]//g')
    echo "Processing $secret"
    CONFIG_MAP_DATA+="    - name: $(jq -r .secret_prefix $CONFIG)${secret_name}\n"
    CONFIG_MAP_DATA+="      type: kubernetes.io/tls\n"
    CONFIG_MAP_DATA+="      namespaces: $(jq -r '.namespaces | @text' $CONFIG)\n"
    CONFIG_MAP_DATA+="      data:\n"

    if [[ "$(jq -r '.vault_secrets_keys | if type=="array" then "yes" else "no" end' $CONFIG)" == "yes" ]] ; then 
        for key in $(jq -r .vault_secrets_keys[] $CONFIG) ; do
            CONFIG_MAP_DATA+="        $(jq -r '[.secret_key_prefix, .secret_key_separator] | join("")' $CONFIG)${key}:\n"
            CONFIG_MAP_DATA+="          path: ${secret}\n"
            CONFIG_MAP_DATA+="          key: ${key}\n"
        done
    else
        for key in $(jq -r '.vault_secrets_keys | keys[]' $CONFIG) ; do
            key_src=$(jq -r ".vault_secrets_keys[\"$key\"]" $CONFIG)
            CONFIG_MAP_DATA+="        $(jq -r '[.secret_key_prefix, .secret_key_separator] | join("")' $CONFIG)${key}:\n"
            CONFIG_MAP_DATA+="          path: ${secret}\n"
            CONFIG_MAP_DATA+="          key: ${key_src}\n"
        done
    fi
done
#echo -e "$CONFIG_MAP_DATA"

# If clusterwide deployment, add clusterroles
if [[ "$CLUSTERWIDE" == "yes" ]] ; then 
    cat $TEMPLATE_DIR/roles-clusterwide.yaml >> $TEMPLATE_DIR/deployment.yaml
else
    cat $TEMPLATE_DIR/roles-namespaceonly.yaml >> $TEMPLATE_DIR/deployment.yaml
fi

# Generate configs from Template
sed -e "s/###APPNAME###/$APPNAME/" \
    -e "s/###NAMESPACENAME###/$NAMESPACENAME/" \
    -e "s/###APPROLE_ID_HASH###/$APPROLE_ID_HASH/" \
    -e "s/###APPROLE_SECRET_ID_HASH###/$APPROLE_SECRET_ID_HASH/" \
    -e "s/###CI_ENVIRONMENT_SLUG###/$CI_ENVIRONMENT_SLUG/" \
    -e "s/###CI_PIPELINE_ID###/$CI_PIPELINE_ID/" \
    -e "s@###VAULTADDR###@$VAULT_ADDRESS@" \
    $TEMPLATE_DIR/deployment.yaml > deployment.tmp

# Split tmp file and insert some code in the middle
# I was unable to 100% substitude yaml code with sed
sed -n '1,/^###CONFIGMAPDATA###/p' deployment.tmp > deployment.yaml && \
echo -e "$CONFIG_MAP_DATA" >> deployment.yaml && \
sed -n '/^###CONFIGMAPDATA###/,$p' deployment.tmp >> deployment.yaml && \
sed -i '/^###CONFIGMAPDATA###/d' deployment.yaml

# Delete some dangerous fragments
rm deployment.tmp
