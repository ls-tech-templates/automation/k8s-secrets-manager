#!/bin/sh
#
# Default values if not exist from GitLab Environment

CI_PIPELINE_ID=${CI_PIPELINE_ID:-undefined}
CLUSTERWIDE=${CLUSTERWIDE:-yes}

# If clusterwide deployment, add clusterroles
if [[ "$CLUSTERWIDE" == "yes" ]] ; then 
    kubectl delete pods,services,secrets,deployments,serviceaccounts,clusterroles,clusterrolebindings,roles,rolebindings,configmaps -l pipeline_id=$CI_PIPELINE_ID --all-namespaces
else
    kubectl delete pods,services,secrets,deployments,serviceaccounts,roles,rolebindings,configmaps -l pipeline_id=$CI_PIPELINE_ID
fi
