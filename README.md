# Secret Manager Template

This is CI/CD integration template for Kubernetes
Secrets Manager.
[Secrets Manager](https://github.com/tuenti/secrets-manager) is tool for managing Kubernetes
secrets with [HashiCorp's Vault](https://www.vaultproject.io/). 
It automatically handles secrets and renews access tokens.

This integrations is for Developers to handle some uncomfortable stuff arount Vault authentification
and ConfigMap prepare.

# Goal

Deploy SSL/TLS certificates from Vault to Kubernetes Secrets automatically.

# How to use it

First, click on fork on the top right corner and create fork of this template.

## Configuration

### config.json

First of all you have to create config.json.
Use `config.json-example` as example how to configure your own.
Place `config.json` in the root of git repository.

### Vault Access

This template is build for AppRole auth. 
Contact your Vault administrator who provide you `role-id` and `secret-id`

### CI/CD Variables

If you have no idea what it is. Visit [GitLab Documentation](https://docs.gitlab.com/ee/ci/variables/)

You neet to set quite few of them

* `APPNAME`, name of the secret-manager app, could be `secret-manager`
* `APPROLE_ID`, Vault role-id
* `APPROLE_SECRET_ID`, Vault secret-id for that role
* `VAULT_ADDRESS`, address of the Vault server, like `https://localhost:8200`
* `CLUSTERWIDE`, determine if secret-manager manage whole cluster or just current namespace (default:yes)

### Kubernetes ServiceAccount

Before you add Kubernetes cluster integration to the project in the next step, you have to create ServiceAccount for deployment.
SecretManager need extensive access rights becouse it handle Secrets across namespaces.
It's recommended to create separet namespace for this purpose

Create namespace and service account
```
kubectl create namespace secrets-manager
kubectl create -n secrets-manager serviceaccount gitlab-super
```

Create ClusterRoleBinding

```
kubectl create clusterrolebinding gitlab-super --clusterrole=cluster-admin --serviceaccount=secrets-manager:gitlab-super
```

In the next step you will need token for that gitlab-super serviceaccount.

```
kubectl get secret -n secrets-manager -o json $(kubectl get -n secrets-manager sa gitlab-super -o json | jq -r .secrets[0].name) | jq -r '.data.token' | base64 -d
```

and CA.crt

```
kubectl get secret -n secrets-manager -o json $(kubectl get -n secrets-manager sa gitlab-super -o json | jq -r .secrets[0].name) | jq -r '.data["ca.crt"]' | base64 -d
```

### Kubernetes Cluster

Gitlab has nice integration with Kubernetes. Please visit [GitLab Kubernetes Documentation](https://docs.gitlab.com/ee/user/project/clusters/)

## Monitoring

Monitor messages with loglevel `error`. Is it important to know when something goes wrong with certificates deployment.

For example when token expire you will get this message:
```
{"level":"error","msg":"failed to fetch token: Error making API request.\n\nURL: GET https://vault:8200/v1/auth/token/lookup-self\nCode: 403. Errors:\n\n* permission denied","time":"2019-05-03T11:44:30Z"}
```

# Links & Talks

Feel free to modify this template to meet your need. You may need different things to approach.

All of theese is based on various projects, please visit and learn:
* [Secrets Manager](https://github.com/tuenti/secrets-manager)
* [HashiCorp's Vault](https://www.vaultproject.io/)


